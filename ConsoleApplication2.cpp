﻿// ConsoleApplication2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение про
#define _CRT_SECURE_NO_WARNINGS
#include< stdio.h>
#include< conio.h>
#include< string.h>
#include <stdlib.h>
#include <Windows.h>
struct node
{
	char sym[10];
	float jmo;
	int arr[20];
	int top;
}s[20];

typedef struct node node;

void prints(int a, int n, node s[])
{
	int i;
	for (i = a; i <= n; i++)
	{
		printf("\n%s\t%f", s[i].sym, s[i].jmo);
	}
}

void shannon(int a, int n, node s[])
{
	float p1 = 0, p2 = 0, d1 = 0, d2 = 0;
	int i, d, k, j;
	if ((a + 1) == n || a == n || a > n)
	{
		if (a == n || a > n)
			return;
		s[n].arr[++(s[n].top)] = 0;
		s[a].arr[++(s[a].top)] = 1;
		return;
	}
	else
	{
		for (i = a; i <= n - 1; i++)
			p1 = p1 + s[i].jmo;
		p2 = p2 + s[n].jmo;
		d1 = p1 - p2;
		if (d1 < 0)
			d1 = d1 * -1;
		j = 2;
		while (j != n - a + 1)
		{
			k = n - j;
			p1 = p2 = 0;
			for (i = a; i <= k; i++)
				p1 = p1 + s[i].jmo;
			for (i = n; i > k; i--)
				p2 = p2 + s[i].jmo;
			d2 = p1 - p2;
			if (d2 < 0)
				d2 = d2 * -1;
			if (d2 >= d1)
				break;
			d1 = d2;
			j++;
		}
		k++;
		for (i = a; i <= k; i++)
			s[i].arr[++(s[i].top)] = 1;
		for (i = k + 1; i <= n; i++)
			s[i].arr[++(s[i].top)] = 0;
		shannon(a, k, s);
		shannon(k + 1, n, s);
	}
}

void main()
{

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);


	int c, i, j;
	float x, total = 0;
	char ms[10];
	node temp;

	printf("Введіть кількість символів для обрахунку: ");
	scanf("%d", &c);
	for (i = 0; i < c; i++)
	{
		printf("Ввід символів %d ---> ", i + 1);
		scanf("%s", ms);
		strcpy(s[i].sym, ms);
	}
	for (i = 0; i < c; i++)
	{
		printf("\n\tВвід Ймовірностей для %s ---> ", s[i].sym);
		scanf("%f", &x);
		s[i].jmo = x;
		total = total + s[i].jmo;

	}
	s[i].jmo = 1 - total;
	for (j = 1; j <= c - 1; j++)
	{
		for (i = 0; i < c - 1; i++)
		{
			if ((s[i].jmo) > (s[i + 1].jmo))
			{
				temp.jmo = s[i].jmo;
				strcpy(temp.sym, s[i].sym);
				s[i].jmo = s[i + 1].jmo;
				strcpy(s[i].sym, s[i + 1].sym);
				s[i + 1].jmo = temp.jmo;
				strcpy(s[i + 1].sym, temp.sym);
			}
		}
	}
	for (i = 0; i < c; i++)
		s[i].top = -1;

	shannon(0, c - 1, s);
	printf("---------------------------------------------------------------");
	printf("\n\n\n\tСимвол\tЙмовірність\tКод за алгоритмом Шеннона-Фано");
	for (i = c - 1; i >= 0; i--)
	{
		printf("\n\t%s\t%f\t", s[i].sym, s[i].jmo);
		for (j = 0; j <= s[i].top; j++)
			printf("%d", s[i].arr[j]);
	}
	printf("\n---------------------------------------------------------------");
	_getch();
}

	
	


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
